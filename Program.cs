﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/** 
 * Projet : ADN
 * Détails : calcul du nombre d'occurence des bases ACGT
 * Auteur : Maya.G
 * Date : 14/10/22
 * version : 1.0 
 **/
namespace ADN
{
    internal class Program
    {
        static void Main(string[] args)
        {
            StreamReader s;
            s = new StreamReader("G:\\Mon Drive\\2022-2023\\vendredi\\chromosome-11-partial\\chromosome-11-partial.txt");
            String line;
            line = s.ReadLine();
            int nbA = 0;
            int nbT = 0;
            int nbC = 0;
            int nbG = 0;
            while (line != null)
            {
                for (int i = 0; i < line.Length; i++)
                {
                    //Console.WriteLine(line[i]);
                    // amélioration: opérateur switch
                    if (line[i] == 'A')
                    {
                        nbA++;
                    }
                    if (line[i] == 'T')
                    {
                        nbT++;
                    }
                    
                    if (line[i] == 'C')
                    {
                        nbC++;
                    }
                    
                    if (line[i] == 'G')
                    {
                        nbG++;
                    }
                }
                               
                line = s.ReadLine();
            }

            Console.WriteLine($"Compteur A = {nbA}");
            Console.WriteLine($"Compteur T = {nbT}");
            Console.WriteLine($"Compteur C = {nbC}");
            Console.WriteLine($"Compteur G = {nbG}");

        }
    }
}
